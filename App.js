import React, { Component, useState } from 'react'
import { Text, StyleSheet, View, SafeAreaView, ScrollView } from 'react-native'
import Header from './components/Header'
import Post from './components/Post'
import Room from './components/Room'
import Story from './components/Story'

export default class App extends Component {

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1 }}
      >
        <View style={styles.container}>
          <ScrollView 
                nestedScrollEnabled={true}>
            <Header />
            <Room />
            <Story />
            <Post />
          </ScrollView>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
})
