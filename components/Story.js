import React, { useState } from 'react'
import { ImageBackground, ScrollView, StyleSheet, Text, View, Image, FlatList} from 'react-native'
// import { Colors } from 'react`-native/Libraries/NewAppScreen'

const Story = () => {
    
    const {imageBg, imageStyle, imageContainer, container, textName} = styles

    const [storyData, setStoryData] = useState(
        [
            {
                key : 1,
                name : 'Rin Sopheary',
                profile : require('../assets/images/pheary.jpg'),
                storyImg : require('../assets/images/s1.png'),
            },
            {
                key : 2,
                name : 'Ly Manny',
                profile : require('../assets/images/manny.jpg'),
                storyImg : require('../assets/images/s2.png'),
            },
            {
                key : 3,
                name : 'Vey Panha',
                profile : require('../assets/images/panha.jpg'),
                storyImg : require('../assets/images/s5.png'),
            },
            {
                key : 4,
                name : 'Sat Ouhsa',
                profile : require('../assets/images/ousha.jpg'),
                storyImg : require('../assets/images/s3.png'),
            },
            {
                key : 21,
                name : 'Khean Visal',
                profile : require('../assets/images/visal.jpg'),
                storyImg : require('../assets/images/s1.png'),
            },
            {
                key : 31,
                name : 'You Taro',
                profile : require('../assets/images/taro.jpg'),
                storyImg : require('../assets/images/s4.png'),
            },
            {
                key : 41,
                name : 'Rin Sopheary',
                profile : require('../assets/images/pheary.jpg'),
                storyImg : require('../assets/images/s1.png'),
            },
        ]
    )

    // const header = ({
    //     key : 41,
    //     name : 'Rin Sopheary',
    //     profile : require('../assets/images/pheary.jpg'),
    //     storyImg : require('../assets/images/s1.png'),
    // },)

    return (
        // <ScrollView
        //     horizontal = {true}
        //     showsHorizontalScrollIndicator = {false}
        //     style={container}
        // >
            
            

            <FlatList 
            nestedScrollEnabled = {true}

                horizontal ={true}
                showsHorizontalScrollIndicator = {false}
                showsVerticalScrollIndicator = {false}
                // stickyHeaderIndices = {[0]}
                // ListHeaderComponent={()=>(<View style={styles.big}>
                // <ImageBackground source={item.storyImg} style={imageBg}>
                //     <View style={imageContainer}>
                //         <Image 
                //             source = {item.profile}
                //             style={imageStyle}
                //         />
                //     </View>
                //     <View style={textName}>
                //         <Text style={{ color: 'white' }}>{item.name}</Text>
                //     </View>
                // </ImageBackground>
                // </View>)}
                // stickyHeaderIndices={[0]}
                data ={storyData}
                style={container}
                renderItem = {({item}) => (
                    <View style={styles.big}>
                    <ImageBackground source={item.storyImg} style={imageBg}>
                        <View style={imageContainer}>
                            <Image 
                                source = {item.profile}
                                style={imageStyle}
                            />
                        </View>
                        <View style={textName}>
                            <Text style={{ color: 'white' }}>{item.name}</Text>
                        </View>
                    </ImageBackground>
                    </View>  
                )}
            />

            
        // </ScrollView>
    )
}

export default Story

const styles = StyleSheet.create({
    big : {
        marginLeft:10
    },
    container : {
        paddingVertical : 10,
        borderBottomWidth : 10,
        borderColor: '#D3D3D3',
    },
    imageBg : {
        // width : '100%',
        // height : '100%',
        // resizeMode : "cover"
        padding : 5,
    },
    imageContainer : {
        paddingVertical : 5,
        // paddingTop: 5,
        // paddingLeft :5,
        width : 100,
        height : 170,
    },
    imageStyle : {
        width : 45,
        height : 45,
        borderRadius : 45/2,
        // paddingLeft: 30
    },
    textName : {
        bottom : 0,
        alignItems : "center",
        paddingBottom : 5
    }
})
