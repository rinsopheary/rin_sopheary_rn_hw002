import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, View, Image, FlatList } from 'react-native'

const Room = () => {


    const [people, setPeople] = useState(
        [
            {
                key: 1,
                name: 'Rin Sopheary',
                image: require('../assets/images/pheary.jpg')
            },
            {
                key: 2,
                name: 'Ly Manny',
                image: require('../assets/images/manny.jpg')
            },
            {
                key: 3,
                name: 'You Taro',
                image: require('../assets/images/taro.jpg')
            },
            {
                key: 4,
                name: 'Vey Panha',
                image: require('../assets/images/panha.jpg')
            },
            {
                key: 5,
                name: 'Sat Ouhsa',
                image: require('../assets/images/ousha.jpg')
            },
            {
                key: 6,
                name: 'Mom Reksmey',
                image: require('../assets/images/reksmey.jpg')
            },
            {
                key: 7,
                name: 'Nget Sonyta',
                image: require('../assets/images/nyta.jpg')
            },
            {
                key: 8,
                name: 'Khean Visal',
                image: require('../assets/images/visal.jpg')
            },
            {
                key: 9,
                name: 'Ly Manny',
                image: require('../assets/images/manny.jpg')
            },
        ]
    );
    const { container, imageStyle, imageContainer } = styles

    return (
        // <ScrollView 
        // style={container}
        // horizontal = {true}
        // showsHorizontalScrollIndicator = {false}
        // >
        <FlatList
        nestedScrollEnabled = {true}
            horizontal={true}
            style={container}
            data={people}
            renderItem={({ item }) => (
                <View style={imageContainer}>
                    <Image
                        source={item.image}
                        style={imageStyle}
                    />
                </View>
            )}
        />

        // </ScrollView>
    )
}

export default Room

const styles = StyleSheet.create({
    container: {
        borderTopWidth: 10,
        borderBottomWidth: 10,
        borderColor: '#D3D3D3',
        paddingVertical: 5,
    },
    imageContainer: {
        marginLeft: 20,
    },
    imageStyle: {
        width: 45,
        height: 45,
        borderRadius: 45 / 2
    },
})
