import React, { useState } from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Alert, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'


const Post = () => {
    const [data, setData] = useState(
        [
            {
                key: 1,
                name: 'ABA Bank',
                profile: require("../assets/images/aba.jpg"),
                content: "ត្រូវការបង់បុព្វលាភរ៉ាប់រងមែនទេ? មានតែ ABA Mobile មួយ គ្រប់គ្រាន់ហេីយ! អ្នកអាចបង់បុព្វលាភធានារ៉ាប់រង ដោយឥតគិតថ្លៃសេវា គ្រប់ពេលវេលា គ្រប់ទីកន្លែង។ ចូលទៅកាន់ម៉ឺនុយទូទាត់ប្រាក់ ជ្រេីសក្រុមហ៊ុនធានារ៉ាប់រង ជាការស្រេច! ",
                imageContent: require("../assets/images/abapost.jpg"),
                like : 0
            },
            {
                key: 2,
                name: 'CKCC: Cambodia-Korea Cooperation Center',
                profile: require("../assets/images/ckcc.png"),
                content: "Autumn in Korea is one of the most marvelous vistas you will get to experience in your life-time😍. The warm hues that surround every street of Korea ought not to be missed. From September to November, when the temperature is around 20°C, the yellow and orange leaves🍁 of maple and gingko descending on the ground creates a serene atmosphere which brings you to you spatio-temporelles Utopia. ",
                imageContent: require("../assets/images/ckccpost.jpg"),
                like : 0
            },
            {
                key: 3,
                name: 'Hor Siekny',
                profile: require("../assets/images/siekny.jpg"),
                content: "A good opportunity for you to improve IT skills.",
                imageContent: require("../assets/images/hrdpost.jpg"),
                like : 0
            },
            {
                key: 4,
                name: 'Ousha Sat',
                profile: require("../assets/images/ousha.jpg"),
                content: "😂😏",
                imageContent: require("../assets/images/oushapost.jpg"),
                like : 0
            },
            {
                key: 5,
                name: 'Panha Vey',
                profile: require("../assets/images/panha.jpg"),
                content: "ហានិភ័យ ....... 🙄",
                imageContent: require("../assets/images/panhapost.jpg"),
                like : 0
            },

        ]
    );

    // const [like, setLike] = useState(0)

    // const handlerBtnLike = () =>{
    //     setData([
    //         {like : like+1}
    //     ])
    // }
    console.log({ data });

    const { container, imageStyle, headerText, iconStyle, head2, box1, box2, box3, box2Img, box4 } = styles
    return (

        // <View>

            <FlatList
                nestedScrollEnabled={true}
                data={data}
                renderItem={({ item }) =>
                    (
                        <View style={container}>
                            <View style={styles.postHeader}>
                                <View style={head2}>
                                    <Image source={item.profile} style={imageStyle}></Image>
                                    <View style={headerText}>
                                        <Text>{item.name}</Text>
                                    </View>
                                </View>
                                <View style={iconStyle}>
                                    <TouchableOpacity
                                        onPress={() => Alert.alert(`${item.content}`)}
                                    >
                                        <FontAwesome5 name={'ellipsis-h'}/>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={box1}>
                                <Text numberOfLines={2}>{item.content}</Text>
                            </View>
                            <View style={box2}>
                                <Image
                                    source={item.imageContent}
                                    style={box2Img}
                                />
                            </View>
                            <View style={box3}>
                            <Text>Like</Text>
                                <Text>Comment</Text>
                                <Text>Share</Text>
                            </View>

                            <View style={box4}>
                                <TouchableOpacity
                                    // onPress = {handlerBtnLike}
                                >
                                    <FontAwesome5 name={'thumbs-up'} size={20} />
                                </TouchableOpacity>
                                <FontAwesome5 name={'comment'} size={20} />
                                <FontAwesome5 name={'share-square'} size={20} />
                            </View>
                        </View>
                    )}
            />
        // </View>
    )
}

export default Post

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    postHeader: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        // alignItems: "center",

    },
    head2: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
    },
    imageStyle: {
        width: 45,
        aspectRatio: 1,
        borderRadius: 45 / 2
    },
    headerText: {
        marginLeft: 10
    },

    box1: {
        marginTop: 10,
    },

    box2: {
        marginTop: 10,
        height: 200,
    },
    box2Img: {
        width: '100%',
        height: '100%',
        resizeMode: "cover"
    },

    box3: {
        padding: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomWidth: 1,
        borderColor: '#D3D3D3'
    },

    box4: {
        padding: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomWidth: 10,
        borderColor: '#D3D3D3',
    }

})

// import React, { Component } from 'react'
// import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import { Alert, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

// export default class Post extends Component {
//     constructor(props) {
//         super(props)
    
//         this.state = {
//              data : [
//                           {
//                                 key: 1,
//                                 name: 'ABA Bank',
//                                 profile: require("../assets/images/aba.jpg"),
//                                 content: "ត្រូវការបង់បុព្វលាភរ៉ាប់រងមែនទេ? មានតែ ABA Mobile មួយ គ្រប់គ្រាន់ហេីយ! អ្នកអាចបង់បុព្វលាភធានារ៉ាប់រង ដោយឥតគិតថ្លៃសេវា គ្រប់ពេលវេលា គ្រប់ទីកន្លែង។ ចូលទៅកាន់ម៉ឺនុយទូទាត់ប្រាក់ ជ្រេីសក្រុមហ៊ុនធានារ៉ាប់រង ជាការស្រេច! ",
//                                 imageContent: require("../assets/images/abapost.jpg"),
//                                 like : 0
//                             },
//                             {
//                                 key: 2,
//                                 name: 'CKCC: Cambodia-Korea Cooperation Center',
//                                 profile: require("../assets/images/ckcc.png"),
//                                 content: "Autumn in Korea is one of the most marvelous vistas you will get to experience in your life-time😍. The warm hues that surround every street of Korea ought not to be missed. From September to November, when the temperature is around 20°C, the yellow and orange leaves🍁 of maple and gingko descending on the ground creates a serene atmosphere which brings you to you spatio-temporelles Utopia. ",
//                                 imageContent: require("../assets/images/ckccpost.jpg"),
//                                 like : 0
//                             },
//                             {
//                                 key: 3,
//                                 name: 'Hor Siekny',
//                                 profile: require("../assets/images/siekny.jpg"),
//                                 content: "A good opportunity for you to improve IT skills.",
//                                 imageContent: require("../assets/images/hrdpost.jpg"),
//                                 like : 0
//                             },
//                             {
//                                 key: 4,
//                                 name: 'Ousha Sat',
//                                 profile: require("../assets/images/ousha.jpg"),
//                                 content: "😂😏",
//                                 imageContent: require("../assets/images/oushapost.jpg"),
//                                 like : 0
//                             },
//                             {
//                                 key: 5,
//                                 name: 'Panha Vey',
//                                 profile: require("../assets/images/panha.jpg"),
//                                 content: "ហានិភ័យ ....... 🙄",
//                                 imageContent: require("../assets/images/panhapost.jpg"),
//                                 like : 0
//                             },
                
//                         ]
//         }
//     }

//     handlerLike = () =>{
//         // console.log(this.state.like);
//         this.setState(state => {
//             const list = state.data.map(item => {item+1});
       
//             return {
//               list,
//             };
//           });
    
//     }
    
//     render() {
//         const { container, imageStyle, headerText, iconStyle, head2, box1, box2, box3, box2Img, box4 } = styles
//         return (
//                         <FlatList
//                 nestedScrollEnabled={true}
//                 data={this.state.data}
//                 renderItem={({ item }) =>
//                     (
//                         <View style={container}>
//                             <View style={styles.postHeader}>
//                                 <View style={head2}>
//                                     <Image source={item.profile} style={imageStyle}></Image>
//                                     <View style={headerText}>
//                                         <Text>{item.name}</Text>
//                                     </View>
//                                 </View>
//                                 <View style={iconStyle}>
//                                     <TouchableOpacity
//                                         onPress={() => Alert.alert(`${item.content}`)}
//                                     >
//                                         <FontAwesome5 name={'ellipsis-h'}/>
//                                     </TouchableOpacity>
//                                 </View>
//                             </View>

//                             <View style={box1}>
//                                 <Text numberOfLines={2}>{item.content}</Text>
//                             </View>
//                             <View style={box2}>
//                                 <Image
//                                     source={item.imageContent}
//                                     style={box2Img}
//                                 />
//                             </View>
//                             <View style={box3}>
//                             <Text>{item.like}Likes</Text>
//                                 <Text>Comment</Text>
//                                 <Text>Share</Text>
//                             </View>

//                             <View style={box4}>
//                                 <TouchableOpacity
//                                     onPress = {this.handlerLike}
//                                 >
//                                     <FontAwesome5 name={'thumbs-up'} size={20} />
//                                 </TouchableOpacity>
//                                 <FontAwesome5 name={'comment'} size={20} />
//                                 <FontAwesome5 name={'share-square'} size={20} />
//                             </View>
//                         </View>
//                     )}
//             />
//         )
//     }
// }
// const styles = StyleSheet.create({
//     container: {
//         padding: 10
//     },
//     postHeader: {
//         flexDirection: "row",
//         alignItems: "center",
//         justifyContent: "space-between",
//         // alignItems: "center",

//     },
//     head2: {
//         flexDirection: "row",
//         alignItems: "center",
//         justifyContent: "space-between",
//     },
//     imageStyle: {
//         width: 45,
//         aspectRatio: 1,
//         borderRadius: 45 / 2
//     },
//     headerText: {
//         marginLeft: 10
//     },

//     box1: {
//         marginTop: 10,
//     },

//     box2: {
//         marginTop: 10,
//         height: 200,
//     },
//     box2Img: {
//         width: '100%',
//         height: '100%',
//         resizeMode: "cover"
//     },

//     box3: {
//         padding: 10,
//         flexDirection: "row",
//         justifyContent: "space-between",
//         borderBottomWidth: 1,
//         borderColor: '#D3D3D3'
//     },

//     box4: {
//         padding: 20,
//         flexDirection: "row",
//         justifyContent: "space-between",
//         borderBottomWidth: 10,
//         borderColor: '#D3D3D3',
//     }
// })
