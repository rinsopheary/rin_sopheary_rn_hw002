import React, { useState } from 'react'
import { StyleSheet, Text, View , Image, Button, TouchableOpacity, Alert, TextInput} from 'react-native'

const Header = () => {

    const {headerContainer, fbText, imageStyle, btnStyle, btnText} = styles

    const [username, setUsername] = useState("Pheary")
    const [description, setDescription] = useState("New Description")

    const postHandler = () => {
        // Alert.alert(`${username} and this is ${description}`);
        Alert.alert(
            'Not yet done teacher, XD.'
        )
    }

    return (
        <View style={headerContainer}>
            <Text style = {fbText}>Facebook</Text>
            <View style = {styles.postHeader}>
                <Image 
                    source={require('../assets/images/pheary.jpg')}
                    style={imageStyle}
                />
                <TouchableOpacity style={btnStyle} onPress={postHandler}>
                    <View>
                        <Text style={btnText}>What's on your mind?</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    
    headerContainer : {
        padding : 10
    },
    fbText : {
        fontSize : 30,
        color : '#3A559F',
        fontWeight : "bold"
    },
    postHeader : {
        marginTop : 20,
        flexDirection : "row"
    },
    imageStyle : {
        width : 40,
        height : 40,
        borderRadius : 40/2
    },
    btnStyle : {
        flex : 1,
        borderRadius: 40,
        borderWidth : 0.5,
        justifyContent: "center",
        alignItems: "center",
        marginLeft : 10,
        backgroundColor : 'white'
    },
    btnText : {
        color : '#3A559F',
        fontWeight : "bold",
        fontSize : 15
    }
})
